/**
 * App Reducers
 */
import { combineReducers } from "redux";

import WeatherReducer from "./WeatherReducer";


const reducers = combineReducers({


    weather: WeatherReducer,


});

export default reducers;

