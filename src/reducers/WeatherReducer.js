/**
 * Reducers
 */
import {
    GET_WEATHER,
    GET_WEATHER_SUCCESS,
    GET_WEATHER_FAIL,
    GET_WEATHER_OTHER,
    GET_WEATHER_OTHER_SUCCESS,
    GET_WEATHER_FAIL_OTHER
} from "actions/types";
import * as moment from "moment";

const INIT_STATE = {
    loadingWeather: false,
    weatherPayload: [],
    cityId: null,
    otherDaysPayload: [],
    otherDaysLoading: false,



};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_WEATHER:
            return {
                ...state,
                loadingWeather: true};

        case GET_WEATHER_SUCCESS:
            return {
                ...state,
                weatherPayload: action.payload,
                loadingWeather: false
            };

        case GET_WEATHER_FAIL:
            return {
                ...state,
                loadingWeather: false
            };

        case GET_WEATHER_OTHER:
            return {
                ...state,
                otherDaysLoading: true
            };

        case GET_WEATHER_OTHER_SUCCESS:
            return {
                ...state,
                otherDaysPayload: action.payload.list.filter(item => {
                    if(moment.unix(item.dt).format('HH:mm') === '11:00' && moment().format('DD/MM/YYYY') !== moment.unix(item.dt).format('DD/MM/YYYY')){
                        return item
                    }
                }),
                otherDaysLoading: false,

            };

        case GET_WEATHER_FAIL_OTHER:
            return {
                ...state,
                otherDaysLoading: true
            };

        default:
            return { ...state };
    }
};
