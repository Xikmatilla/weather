import React, {useState, useEffect} from 'react';
import Image from '../../assets/cities/Toshkent.jpg'
import SpinnerComponent from "../spinner/spinner-component";
import WeatherAbout from "../weather-about/WeatherAbout";
import Moment from "react-moment";
import {SECTIONS } from "./image";
import {citiesData} from "../../containers/cities";

const BannerComponent = ({name, loading, weatherMain}) => {


    var d = new Date();

    const [image, setImage] = useState(null);

    useEffect(()=> {


    },[]);



    return(

        <div>
            {!loading ? (
                    <div className="banner-container">
                        <div className="banner-section" style={{ backgroundImage: `url(${Image})`}}>
                            <h1>{name}</h1>
                             <h4>
                                 <Moment
                                     className="weather-momnet"
                                     format="YYYY/MM/DD"
                                     date={d} />
                             </h4>
                                <WeatherAbout weatherMain={weatherMain} />


                        </div>
                    </div>

                ) : (
                    <SpinnerComponent/>
                )}

        </div>
    )
};

export default BannerComponent;