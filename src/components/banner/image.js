export const SECTIONS = [
    {
        title: 'Toshkent',
        file: require('../../assets/cities/Toshkent.jpg'),
    },
    {
        title: 'Andijan',
        file: require('../../assets/cities/Andijan.jpg'),
    },

    {
        title: 'Fergana',
        file: require('../../assets/cities/Fergana.jpg'),
    },

    {
        title: 'Bukhara',
        file: require('../../assets/cities/Bukhara.jpg'),
    },

    {
        title: 'Jizzakh',
        file: require('../../assets/cities/Jizzakh.jpg'),
    },

    {
        title: 'Naman',
        file: require('../../assets/cities/Naman.jpg'),
    },

    {
        title: 'Xorazm',
        file: require('../../assets/cities/Bukhara.jpg'),
    },

    {
        title: 'Toshkent',
        file: require('../../assets/cities/Xorazm.jpg'),
    },

    {
        title: 'Qashqadaryo',
        file: require('../../assets/cities/Qashqadaryo.jpg'),
    },
];
