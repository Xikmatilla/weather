import React from 'react';


const SpinnerComponent = () => {


    return(
        <div className="loading">
            <div className="circle"></div>
            <div className="circle"></div>
            <div className="circle"></div>
            <div className="circle"></div>
        </div>
    )
};

export default SpinnerComponent;