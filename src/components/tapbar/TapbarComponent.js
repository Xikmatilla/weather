import React from 'react';



const TapbarComponent = ({cities, HandlerCity, cityId}) => {


    return(
        <div className="tapbar-container">
            {
                cities && cities.map((city, index)=>
                    <ul key={index} className="tapbar-section">
                        <li
                         className={cityId === city.id ? 'active' : 'noActive'}
                        onClick={()=> HandlerCity(city.id)}
                        >{city.title}</li>
                    </ul>
                )
            }

        </div>
    )
};

export default TapbarComponent;