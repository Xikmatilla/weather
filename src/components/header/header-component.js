import React from 'react';
import logo from '../../assets/logo.png'


const HeaderComponent = () => {


    return(
        <div className="header-section">
            <div className="logo-blog">
                <img src={logo} alt="logo"/>
            </div>

            <div className="text-blog">
                <h1>WEATHER SPA</h1>
            </div>
        </div>
    )
};

export default HeaderComponent;