import React from 'react';
import moment from 'moment';

import SpinnerComponent from "../spinner/spinner-component";


const OtherDaysComponent = ({loading, item}) => {
    debugger
    return(

        <div>
            {
                !loading && item ?(
                    <div className="Other-days-component">
                        {
                            item.filter((item, index )=> index < 4)
                                .map((days, key) => (
                                <div
                                    key={key}
                                    className="other-days-section">
                                    <div className="blod-card">
                                        <h1>{moment.unix(days.dt).format('dddd')}</h1>
                                        <span>{days.main.temp > 0 ? '+' : ''}{days.main.temp.toFixed(0) / 10}</span>
                                        <br/>
                                        <span>{days.weather[0].main}</span>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                ) : (
                    <div id="loading"></div>
                )
            }

        </div>
    )
};

export default OtherDaysComponent;