import React from 'react';
import {useSelector} from "react-redux";
import Moment from 'react-moment';
import moment from 'moment';


const WeatherAbout = ({weatherMain}) => {


    const { loading, weather } = useSelector(({weather })=> ({
        loading: weather.loadingWeather,
        weather: weather.weatherPayload,
    }));


    return(
        <div className="weather-container">

            <div className="weather-temp">
                {

                     weatherMain &&   <h1>{weatherMain.temp > 0 ? '+' : ''}{weatherMain.temp.toFixed(0)/10}</h1>

                }


            </div>

            <div className="weather-center">

            </div>


            {!loading && weatherMain &&
            <div className="weather-mean">
                <ul className="weather-seaction">
                    <li>humidity : {weatherMain.humidity}</li>
                    <li>pressure : {weatherMain.pressure}</li>
                    <li>wind: {weather.wind.speed} m/s {weather.wind.deg}</li>
                    <li>sunrise: {moment.unix(weather.sys.sunrise).format('hh:mm')}</li>
                    <li>sunrise: {moment.unix(weather.sys.sunset).format('hh:mm')}</li>
                </ul>

            </div>
            }
        </div>
    )
};

export default WeatherAbout;