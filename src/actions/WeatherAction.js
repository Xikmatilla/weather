

/**
 * Actions
 */
import {
    GET_WEATHER,
    GET_WEATHER_OTHER
} from "./types";




// getWeather Actions
export const getWeather = ({payload}) => ({
    type: GET_WEATHER,
    payload: payload
});


// getOtherDays Actions
export const getOtherDays = ({payload}) => ({
    type: GET_WEATHER_OTHER,
    payload: payload
});
