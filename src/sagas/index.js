/**
 * Root Sagas
 */
import { all, fork } from "redux-saga/effects";



import watcherAuth from "./watchers/weatherSaga";

export default function* rootSaga() {
    yield all([
        fork(watcherAuth),
    ]);
}
