/* eslint-disable no-empty */
import { takeLatest, call, put } from "redux-saga/effects";
import * as types from "actions/types";
import  api  from "services/api/index";
import config from 'config';


function* workerGetWeather({ payload }) {

    try {

        const { data } = yield call(api.request.get, `/data/2.5/weather?id=${payload}&APPID=${config.API_KEY}&units${config.API_UNITS}`);
        yield put({
            type: types.GET_WEATHER_SUCCESS,
            payload: data
        });
    } catch (error) {
        yield put({
            type: types.GET_WEATHER_FAIL,
            payload: error.response.data.message
        });
    }
}

function* workerGetOtherDays({ payload }) {

    try {

        const { data } = yield call(api.request.get, `/data/2.5/forecast?id=${payload}&APPID=${config.API_KEY}&units${config.API_UNITS}`);
        yield put({
            type: types.GET_WEATHER_OTHER_SUCCESS,
            payload: data
        });
    } catch (error) {
        yield put({
            type: types.GET_WEATHER_FAIL_OTHER,
            payload: error.response.data.message
        });
    }
}
export default function* weatherSaga() {

    yield takeLatest(types.GET_WEATHER, workerGetWeather);
    yield takeLatest(types.GET_WEATHER_OTHER, workerGetOtherDays);

}
