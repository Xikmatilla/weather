import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getWeather, getOtherDays} from "../actions";



import HeaderComponent from "components/header/header-component";
import TapbarComponent from "components/tapbar/TapbarComponent";
import BannerComponent from "components/banner/banner-component";

import {citiesData} from './cities';
import OtherDaysComponent from "../components/otherDays/OtherDays";


const HomePage = () => {


    const [cities, setcities] = useState(citiesData);
    const [cityId, setCityId] = useState(1512569);


    const dispatch = useDispatch();
    const { loading, weather, otherDays, otherLoading} = useSelector(({weather })=> ({
        loading: weather.loadingWeather,
        weather: weather.weatherPayload,
        otherDays: weather.otherDaysPayload,
        otherLoading: weather.otherDaysLoading
    }));


    useEffect(()=> {
        dispatch(getWeather({
                payload: cityId
            }));

        dispatch(getOtherDays({
                payload: cityId
            }));
    }, []);


    const HandlerCity = (id) => {
        setCityId(id);

        dispatch(getWeather({
                payload: id
            }));

        dispatch(getOtherDays({
                payload: id
            }));

    };


    return (
        <div>
            <HeaderComponent/>
            <TapbarComponent
                cities={cities}
                cityId={cityId}
                HandlerCity={HandlerCity}


            />


            <BannerComponent
                name={weather.name}
                loading={loading}
                weatherMain={weather.main}
            />
            <OtherDaysComponent
                item={otherDays}

                loading={otherLoading}

            />
        </div>
    )


};


export default HomePage;