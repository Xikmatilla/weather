export const citiesData = [
    {
        id: 1514588,
        title: 'Andijan'
    },
    {
        id: 1217662,
        title: 'Bukhara'
    },
    {
        id: 1484845,
        title: 'Fergana'
    },
    {
        id: 1513886,
        title: 'Jizzakh'
    },{
        id: 1484843,
        title: 'Xorazm'
    },
    {
        id: 1513157,
        title: 'Namangan'
    },
    {
        id: 1513131,
        title: 'Navoiy'
    },
    {
        id: 1114928,
        title: 'Qashqadaryo'
    },
    {
        id: 1216265,
        title: 'Samarqand'
    },
    {
        id: 1484840,
        title: 'Sirdaryo'
    },
    {
        id: 1114926,
        title: 'Surxondaryo'
    },
    {
        id: 1512569,
        title: 'Tashkent'
    }
];
