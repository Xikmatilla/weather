import React, { useState, useEffect} from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';




import './App.css';
import HomePage from "./containers/HomePage";



function App() {


  return (
    <div className="App">
        <Switch>
            <Route exact path='/' component={HomePage}  />
        </Switch>
    </div>
  );
}

export default App;
